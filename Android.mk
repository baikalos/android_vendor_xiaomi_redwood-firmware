LOCAL_PATH := $(call my-dir)

ifeq ($(TARGET_DEVICE),redwood)

$(info Including firmware for redwood...)

FIRMWARE_IMAGES := $(wildcard $(LOCAL_PATH)/images/*)

$(foreach f, $(notdir $(FIRMWARE_IMAGES)), \
    $(call add-radio-file,images/$(f)))

endif
